# Projekt Selenium - opis i instrukcja

## Pobranie projektu:
0. Przejdźcie do zakładki Downloads z lewej strony
0. Wybierzcie opcję Download repository
0. Zapiszcie plik gdzieś na dysku - __pamiętajcie, żeby w ścieżce do projektu nie było spacji!__
0. Rozpakujcie pobrany plik zip
0. Otwórzcie projekt poprzez IntlliJ

## Otwieranie istniejących projektów przez IntelliJ:
0. File -> Open...
0. Przechodzicie do lokalizacji projektu, który chcecie otworzyć
0. Jeżeli jest to projekt mavenowy to najprościej jest zaznaczyć plik pom.xml naszego projektu i kliknąć OK
0. IntelliJ zapyta czy otworzyć ten plik jako projekt - klikamy TAK
0. Dajcie mu chwilę na załadowanie plików i ustawień

## Opis struktury projektu:
Wszystkie testy jakie wykonaliśmy w tracie kursu umieściłem w czterech wersjach w odpowiadających im paczkach.

0. _Paczka v0_ - Testy napisane pierwotną metodą, bez stosowania się do Page Object Pattern
0. _Paczka v1_ - Testy napisane przy podstawowych założeniach Page Object Pattern - wydzielenie nowych klas z Page Object'ami, w których znajduje się tylko i wyłącznie wyszukiwanie WebElementów
0. _Paczka v2_ - Udoskonalony Page Object Pattern - w klasach z Page Object'ami zastosowano hermetyzację, czyli atrybuty z WebElement'ami oznaczono jako prywatne i dodano metody, które odpowiadają za interakcję z tymi WebElement'ami
0. _Paczka v3_ - Finalna wersja Page Object Pattern - dodano do metod informacje gdzie będziemy się znajdować na stronie po wykonaniu danej czynności. Tym samym zrefactoryzowano testy aby korzystały z łańcuchowego wywoływania tych metod
0. Testy które celowo mają failować oraz test, który bierze dane z pliku csv znajdują się bezpośrednio w paczce com.testarmy.firstseleniumproject

## Adnotacje Allurowe
Znajdują się we wszystkich testach. Aby działały poprawnie hiperłącza do adnotacji TmsLink oraz Issue należy w resources umieścić plik allure.properties
