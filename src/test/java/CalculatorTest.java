import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Unit tests")
@Story("Unit tests")
public class CalculatorTest {

    @Test
    @TmsLink("TC-1")
    public void multiplicationTest() {
        //given
        int a = 5;
        int b = 10;
        Calculator calculator = new Calculator();

        //when
        int actualResult = calculator.multiply(a, b);

        //then
        Assert.assertEquals(50, actualResult);
    }

    @Test
    @TmsLink("TC-2")
    public void divisionTest() {
        //given
        double a = 5;
        double b = 10;
        Calculator calculator = new Calculator();

        //when
        double actualResult = calculator.divide(a, b);

        //then
        Assert.assertEquals(0.5, actualResult, 0);
    }

}
