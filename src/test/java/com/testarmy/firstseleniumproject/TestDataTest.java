package com.testarmy.firstseleniumproject;

import com.testarmy.firstseleniumproject.config.TestDataReader;
import com.testarmy.firstseleniumproject.pages.v3.HomePage;
import io.qameta.allure.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@Feature("Test Data Test")
@Story("Test Data Sample Tests")
public class TestDataTest extends BaseTest {

    @Rule
    public TestName name = new TestName();

    @Test
    @TmsLink("TC-99")
    @Severity(SeverityLevel.CRITICAL)
    public void shallReadCsvAndUseDataInTest() throws IOException {
        //given
        HomePage homePage = new HomePage();

        String[] testData = TestDataReader.getTestDataForTestName(name.getMethodName());

        String providedEmail = homePage.clickOnContactUsBtn()
                .inputEmailAddress(testData[3])
                .getProvidedEmail();

        //then
        assertEquals("reader@dziala.pl", providedEmail);
    }

}
