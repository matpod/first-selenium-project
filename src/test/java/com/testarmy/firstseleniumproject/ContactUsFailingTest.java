package com.testarmy.firstseleniumproject;

import com.testarmy.firstseleniumproject.config.WaitHelper;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

@Feature("Feature Contact Us Tests - failing ones")
@Story("Failing tests")
public class ContactUsFailingTest extends BaseTest {

    @Before
    public void beforeTest() {
        WebElement btnContactUs = driver.findElement(By.id("contact-link"));
        btnContactUs.click();
        WaitHelper.waitForVisibility(By.id("email"));
        WebElement tfEmail = driver.findElement(By.id("email"));
        tfEmail.sendKeys("test@email.com");
        WebElement tfOrderRef = driver.findElement(By.id("id_order"));
        tfOrderRef.sendKeys("Order reference");
        WebElement tfMessage = driver.findElement(By.id("message"));
        tfMessage.sendKeys("Some message");
    }

    @Test
    @Issue("JIRA-1")
    @TmsLink("TC-18")
    public void incorrectExpectedValue() {
        //given
        WebElement btnSend = driver.findElement(By.id("submitMessage"));

        //when
        btnSend.click();
        WebElement lblError = driver.findElement(By.cssSelector("ol > li"));

        //then
        Assert.assertEquals("Wrong expected value", lblError.getText());
    }

    @Test
    @Issue("JIRA-2")
    @TmsLink("TC-19")
    public void invalidSelector() {
        //given
        Select select = new Select(driver.findElement(By.id("//invalid_selector")));
        select.selectByIndex(1);
        WebElement btnSend = driver.findElement(By.id("submitMessage"));

        //when
        btnSend.click();
        WebElement lblSuccess = driver.findElement(By.cssSelector(".alert.alert-success"));

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", lblSuccess.getText());
    }

    @Test
    @Ignore(value = "Test ignored just because")
    @Issue("JIRA-2")
    @TmsLink("TC-19")
    public void ignoredTest() {
        //given
        Select select = new Select(driver.findElement(By.id("//invalid_selector")));
        select.selectByIndex(1);
        WebElement btnSend = driver.findElement(By.id("submitMessage"));

        //when
        btnSend.click();
        WebElement lblSuccess = driver.findElement(By.cssSelector(".alert.alert-success"));

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", lblSuccess.getText());
    }

}
