package com.testarmy.firstseleniumproject.v2;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.pages.v2.HomePage;
import com.testarmy.firstseleniumproject.pages.v2.ProductPage;
import com.testarmy.firstseleniumproject.pages.v2.ShoppingCartPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Shopping Cart Page Tests - with v2 Page Object Pattern")
@Story("Passing tests")
public class ShoppingCartTest extends BaseTest {

    @Test
    @TmsLink("TC-13")
    public void asCustomerIShallAddBlouseToCart() {
        //given
        HomePage homePage = new HomePage();
        homePage.clickOnSecondProduct();
        ProductPage productPage = new ProductPage();
        productPage.clickOnAddToCart();

        //when
        productPage.clickOnProceed();
        ShoppingCartPage shoppingCartPage = new ShoppingCartPage();
        String totalPrice = shoppingCartPage.getTotalPrice();

        //then
        Assert.assertEquals("$29.00", totalPrice);
    }

}
