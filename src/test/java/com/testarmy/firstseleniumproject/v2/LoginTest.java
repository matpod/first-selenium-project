package com.testarmy.firstseleniumproject.v2;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.pages.v2.HomePage;
import com.testarmy.firstseleniumproject.pages.v2.LoginPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Login Page Tests - with v2 Page Object Pattern")
@Story("Passing tests")
public class LoginTest extends BaseTest {

    @Test
    @TmsLink("TC-12")
    public void asUnregisteredUserIShouldNotLogin() {
        //given
        HomePage homePage = new HomePage();
        homePage.clickOnSignInBtn();
        LoginPage loginPage = new LoginPage();
        loginPage.inputEmailAddress("some@email.com");
        loginPage.inputPassword("Password123");

        //when
        loginPage.clickOnLoginBtn();

        //then
        Assert.assertEquals("Authentication failed.", loginPage.getErrorMessage());
    }

}
