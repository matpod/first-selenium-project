package com.testarmy.firstseleniumproject.v2;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.pages.v2.ContactUsPage;
import com.testarmy.firstseleniumproject.pages.v2.HomePage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

@Feature("Contact Us Page Tests - with v2 Page Object Pattern")
@Story("Passing tests")
public class ContactUsTest extends BaseTest {

    private ContactUsPage contactUsPage;

    @Before
    public void beforeTest() {
        HomePage homePage = new HomePage();
        homePage.clickOnContactUsBtn();
        contactUsPage = new ContactUsPage();
        contactUsPage.inputEmailAddress("test@email.com");
        contactUsPage.inputOrderRef("Order reference");
        contactUsPage.inputMessage("Some message");
    }

    @Test
    @TmsLink("TC-10")
    public void asCustomerIShouldNotSendMessageWithoutTitle() {
        //given & when
        contactUsPage.clickOnSend();

        //then
        Assert.assertEquals("Please select a subject from the list provided.", contactUsPage.getErrorMessage());
    }

    @Test
    @TmsLink("TC-11")
    public void asCustomerIShouldSendMessageWithTitleSelected() {
        //given
        contactUsPage.selectFirstSubject();

        //when
        contactUsPage.clickOnSend();

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", contactUsPage.getSuccessMessage());
    }

    @Test
    @TmsLink("TC-30")
    public void asCustomerIShouldSendMessage() {
        //given
        contactUsPage.selectFirstSubject();
        contactUsPage.attachCatImage();

        //when
        contactUsPage.clickOnSend();

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", contactUsPage.getSuccessMessage());
    }

}
