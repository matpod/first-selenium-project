package com.testarmy.firstseleniumproject.v1;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import com.testarmy.firstseleniumproject.pages.v1.HomePage;
import com.testarmy.firstseleniumproject.pages.v1.ProductPage;
import com.testarmy.firstseleniumproject.pages.v1.ShoppingCartPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Shopping Cart Page Tests - with v1 Page Object Pattern")
@Story("Passing tests")
public class ShoppingCartTest extends BaseTest {

    @Test
    @TmsLink("TC-9")
    public void asCustomerIShallAddBlouseToCart() {
        //given
        HomePage homePage = new HomePage();
        homePage.btnSecondProduct.click();
        ProductPage productPage = new ProductPage();
        WaitHelper.waitForVisibility(productPage.btnAddToCart);
        productPage.btnAddToCart.click();
        WaitHelper.waitForVisibility(productPage.btnProceed);

        //when
        productPage.btnProceed.click();
        ShoppingCartPage shoppingCartPage = new ShoppingCartPage();
        WaitHelper.waitForVisibility(shoppingCartPage.lblTotalPrice);
        String totalPrice = shoppingCartPage.lblTotalPrice.getText();

        //then
        Assert.assertEquals("$29.00", totalPrice);
    }

}
