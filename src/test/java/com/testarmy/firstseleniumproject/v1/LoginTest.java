package com.testarmy.firstseleniumproject.v1;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import com.testarmy.firstseleniumproject.pages.v1.HomePage;
import com.testarmy.firstseleniumproject.pages.v1.LoginPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Login Page Tests - with v1 Page Object Pattern")
@Story("Passing tests")
public class LoginTest extends BaseTest {

    @Test
    @TmsLink("TC-6")
    public void asUnregisteredUserIShouldNotLogin() {
        //given
        HomePage homePage = new HomePage();
        homePage.btnSignIn.click();
        LoginPage loginPage = new LoginPage();
        WaitHelper.waitForVisibility(loginPage.tfEmail);
        loginPage.tfEmail.sendKeys("some@email.com");
        loginPage.tfPassword.sendKeys("Password123");

        //when
        loginPage.btnLogin.click();
        WaitHelper.waitForVisibility(loginPage.lblError);

        //then
        Assert.assertEquals("Authentication failed.", loginPage.lblError.getText());
    }

}
