package com.testarmy.firstseleniumproject.v1;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import com.testarmy.firstseleniumproject.pages.v1.ContactUsPage;
import com.testarmy.firstseleniumproject.pages.v1.HomePage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.ui.Select;

@Feature("Contact Us Page Tests - with v1 Page Object Pattern")
@Story("Passing tests")
public class ContactUsTest extends BaseTest {

    private ContactUsPage contactUsPage;

    @Before
    public void beforeTest() {
        HomePage homePage = new HomePage();
        homePage.btnContactUs.click();
        contactUsPage = new ContactUsPage();
        WaitHelper.waitForVisibility(contactUsPage.tfEmail);
        contactUsPage.tfEmail.sendKeys("test@email.com");
        contactUsPage.tfOrderRef.sendKeys("Order reference");
        contactUsPage.tfMessage.sendKeys("Some message");
    }

    @Test
    @TmsLink("TC-7")
    public void asCustomerIShouldNotSendMessageWithoutTitle() {
        //given & when
        contactUsPage.btnSend.click();
        WaitHelper.waitForVisibility(contactUsPage.lblError);

        //then
        Assert.assertEquals("Please select a subject from the list provided.", contactUsPage.lblError.getText());
    }

    @Test
    @TmsLink("TC-8")
    public void asCustomerIShouldSendMessageWithTitleSelected() {
        //given
        Select select = new Select(contactUsPage.ddSubject);
        select.selectByIndex(1);

        //when
        contactUsPage.btnSend.click();
        WaitHelper.waitForVisibility(contactUsPage.lblSuccess);

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", contactUsPage.lblSuccess.getText());
    }

}
