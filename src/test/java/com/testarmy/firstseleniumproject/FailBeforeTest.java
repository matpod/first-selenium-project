package com.testarmy.firstseleniumproject;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@Feature("Feature Failing Before Test")
@Story("Failing tests")
public class FailBeforeTest extends BaseTest {

    @Before
    public void beforeTest() {
        WebElement btnContactUs = driver.findElement(By.id("contact-link"));
        btnContactUs.click();
        WebElement tfEmail = driver.findElement(By.id("invalid_selector"));
        tfEmail.sendKeys("test@email.com");
        WebElement tfOrderRef = driver.findElement(By.id("id_order"));
        tfOrderRef.sendKeys("Order reference");
        WebElement tfMessage = driver.findElement(By.id("message"));
        tfMessage.sendKeys("Some message");
    }

    @Test
    @Issue("JIRA-1")
    @TmsLink("TC-18")
    public void incorrectSelectorInBefordeValue() {
        //given
        WebElement btnSend = driver.findElement(By.id("submitMessage"));

        //when
        btnSend.click();
        WebElement lblError = driver.findElement(By.cssSelector("ol > li"));

        //then
        Assert.assertEquals("Wrong expected value", lblError.getText());
    }
}
