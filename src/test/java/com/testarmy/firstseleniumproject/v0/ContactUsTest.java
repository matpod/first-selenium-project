package com.testarmy.firstseleniumproject.v0;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

@Feature("Feature Contact Us Tests - without Page Object Pattern")
@Story("Passing tests")
public class ContactUsTest extends BaseTest {

    @Before
    public void beforeTest() {
        WebElement btnContactUs = driver.findElement(By.id("contact-link"));
        btnContactUs.click();
        WebElement tfEmail = WaitHelper.waitForVisibility(By.id("email"));
        tfEmail.sendKeys("test@email.com");
        WebElement tfOrderRef = driver.findElement(By.id("id_order"));
        tfOrderRef.sendKeys("Order reference");
        WebElement tfMessage = driver.findElement(By.id("message"));
        tfMessage.sendKeys("Some message");
    }

    @Test
    @TmsLink("TC-4")
    public void asCustomerIShallNotSendMessageWithoutSubject() {
        //given
        WebElement btnSend = driver.findElement(By.id("submitMessage"));

        //when
        btnSend.click();
        WebElement lblError = WaitHelper.waitForVisibility(By.cssSelector("ol > li"));

        //then
        Assert.assertEquals("Please select a subject from the list provided.", lblError.getText());
    }

    @Test
    @TmsLink("TC-5")
    public void asCustomerIShallSendMessageWhenCorrectDataProvided() {
        //given
        Select select = new Select(driver.findElement(By.id("id_contact")));
        select.selectByIndex(1);
        WebElement btnSend = driver.findElement(By.id("submitMessage"));

        //when
        btnSend.click();
        WebElement lblSuccess = WaitHelper.waitForVisibility(By.cssSelector(".alert.alert-success"));

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", lblSuccess.getText());
    }

}
