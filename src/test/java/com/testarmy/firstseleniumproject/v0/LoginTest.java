package com.testarmy.firstseleniumproject.v0;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@Feature("Login Page Tests - without Page Object Pattern")
@Story("Passing tests")
public class LoginTest extends BaseTest {

    @Test
    @TmsLink("TC-3")
    public void asNotRegisteredCustomerIShallNotLogin() {
        //given
        WebElement btnSignIn = driver.findElement(By.className("login"));
        btnSignIn.click();
        WebElement tfEmail = WaitHelper.waitForVisibility(By.id("email"));
        tfEmail.sendKeys("some@email.com");
        WebElement tfPassword = driver.findElement(By.id("passwd"));
        tfPassword.sendKeys("Password123");
        WebElement btnLogin = driver.findElement(By.id("SubmitLogin"));

        //when
        btnLogin.click();
        WebElement lblError = WaitHelper.waitForVisibility(By.cssSelector("ol > li"));

        //then
        Assert.assertEquals("Authentication failed.", lblError.getText());
    }

}
