package com.testarmy.firstseleniumproject.v3;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.pages.v3.HomePage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Shopping Cart Page Tests - with v3 Page Object Pattern")
@Story("Passing tests")
public class ShoppingCartTest extends BaseTest {

    @Test
    @TmsLink("TC-17")
    public void asCustomerIShallAddBlouseToCart() {
        //given
        HomePage homePage = new HomePage();
        String totalPrice = homePage.clickOnSecondProduct()
                .clickOnAddToCart()

                //when
                .clickOnProceed()
                .getTotalPrice();

        //then
        Assert.assertEquals("$29.00", totalPrice);
    }

}
