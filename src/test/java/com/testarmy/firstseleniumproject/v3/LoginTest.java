package com.testarmy.firstseleniumproject.v3;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.pages.v3.HomePage;
import com.testarmy.firstseleniumproject.pages.v3.LoginPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Test;

@Feature("Login Page tests - with v3 Page Object Pattern")
@Story("Passing tests")
public class LoginTest extends BaseTest {

    @Test
    @TmsLink("TC-16")
    public void asUnregisteredUserIShouldNotLogin() {
        //given
        HomePage homePage = new HomePage();
        homePage.clickOnSignInBtn()
                .inputEmailAddress("some@email.com")
                .inputPassword("Password123")

                //when
                .clickOnLoginBtn();
        LoginPage loginPage = new LoginPage();

        //then
        Assert.assertEquals("Authentication failed.", loginPage.getErrorMessage());
    }

}
