package com.testarmy.firstseleniumproject.v3;

import com.testarmy.firstseleniumproject.BaseTest;
import com.testarmy.firstseleniumproject.pages.v3.ContactUsPage;
import com.testarmy.firstseleniumproject.pages.v3.HomePage;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

@Feature("Contact Us Page Tests - with v3 Page Object Pattern")
@Story("Passing tests")
public class ContactUsTest extends BaseTest {

    private ContactUsPage contactUsPage;

    @Before
    public void beforeTest() {
        HomePage homePage = new HomePage();
        contactUsPage = homePage.clickOnContactUsBtn()
                .inputEmailAddress("test@email.com")
                .inputOrderRef("Order reference")
                .inputMessage("Some message");
    }

    @Test
    @TmsLink("TC-14")
    public void asCustomerIShouldNotSendMessageWithoutTitle() {
        //given & when
        contactUsPage.clickOnSend();

        //then
        Assert.assertEquals("Please select a subject from the list provided.", contactUsPage.getErrorMessage());
    }

    @Test
    @TmsLink("TC-15")
    public void asCustomerIShouldSendMessageWithTitleSelected() {
        //given
        contactUsPage.selectFirstSubject()

                //when
                .clickOnSend();

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", contactUsPage.getSuccessMessage());
    }

    @Test
    @TmsLink("TC-20")
    public void asCustomerIShouldSendMessage() {
        //given
        contactUsPage.selectFirstSubject()
                .attachCatImage()

                //when
                .clickOnSend();

        //then
        Assert.assertEquals("Your message has been successfully sent to our team.", contactUsPage.getSuccessMessage());
    }

}
