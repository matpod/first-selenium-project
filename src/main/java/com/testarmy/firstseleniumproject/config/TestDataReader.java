package com.testarmy.firstseleniumproject.config;

import com.opencsv.CSVReaderHeaderAware;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class TestDataReader {

    public static String[] getTestDataForTestName(String testName) throws IOException {
        List<String[]> testData = getTestData();
        for (String[] data : testData) {
            if (data[0].equals(testName)) { //String porównujemy przy pomocy metody equals(), a nie ==
                //ponieważ nazwa testu jest w pierwszej kolumnie dlatego sprawdzamy
                //element o 0 indeksie
                return data;
            }
        }
        throw new IllegalArgumentException("Test data for provided test name does not exist");
    }

    private static List<String[]> getTestData() throws IOException {
        ClassLoader classLoader = DriverHelper.class.getClassLoader();
        String testDataFile = classLoader.getResource("test_data.csv").getFile();
        FileReader fileReader = new FileReader(testDataFile);
        CSVReaderHeaderAware csv = new CSVReaderHeaderAware(fileReader);
        return csv.readAll();
    }

}
