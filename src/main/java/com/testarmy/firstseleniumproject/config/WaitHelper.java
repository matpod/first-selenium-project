package com.testarmy.firstseleniumproject.config;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WaitHelper {

    public static void implicitWait() {
        WebDriver driver = DriverHelper.getDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static WebElement waitForVisibility(By selector) {
        WebDriver driver = DriverHelper.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        return element;
    }

    public static WebElement waitForVisibility(WebElement webElement) {
        WebDriver driver = DriverHelper.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement element = wait.until(ExpectedConditions.visibilityOf(webElement));
        return element;
    }

}
