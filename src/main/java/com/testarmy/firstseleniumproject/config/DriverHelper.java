package com.testarmy.firstseleniumproject.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;

public class DriverHelper {

    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            driver = startDriver();
        }
        return driver;
    }

    private static WebDriver startDriver() {
        ClassLoader classLoader = DriverHelper.class.getClassLoader();
        String pathToFile = classLoader.getResource("chromedriver.exe").getFile();
        File driverFile = new File(pathToFile);

        System.setProperty("webdriver.chrome.driver", driverFile.getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("http://automationpractice.com/index.php");
        return driver;
    }

    public static void closeDriver() {
        if (driver != null) {
            driver.close();
            driver.quit();
            driver = null;
        }
    }

}
