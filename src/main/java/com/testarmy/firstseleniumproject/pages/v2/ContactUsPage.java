package com.testarmy.firstseleniumproject.pages.v2;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import lombok.extern.java.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.File;

@Log
public class ContactUsPage {

    @FindBy(id = "id_contact")
    private WebElement ddSubject;
    @FindBy(id = "email")
    private WebElement tfEmail;
    @FindBy(id = "id_order")
    private WebElement tfOrderRef;
    @FindBy(id = "message")
    private WebElement tfMessage;
    @FindBy(id = "submitMessage")
    private WebElement btnSend;
    @FindBy(css = ".alert.alert-success")
    private WebElement lblSuccess;
    @FindBy(css = "ol > li")
    private WebElement lblError;
    @FindBy(id = "fileUpload")
    private WebElement tfAttachFile;

    public ContactUsPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

    public void selectFirstSubject() {
        log.info("Selecting first subject from drop down");
        Select select = new Select(ddSubject);
        select.selectByIndex(1);
    }

    public void inputEmailAddress(String email) {
        log.info("Entering email address");
        WaitHelper.waitForVisibility(tfEmail);
        tfEmail.sendKeys(email);
    }

    public void inputOrderRef(String orderRef) {
        log.info("Entering order reference");
        WaitHelper.waitForVisibility(tfOrderRef);
        tfOrderRef.sendKeys(orderRef);
    }

    public void inputMessage(String message) {
        log.info("Entering contact message");
        WaitHelper.waitForVisibility(tfMessage);
        tfMessage.sendKeys(message);
    }

    public void clickOnSend() {
        log.info("Clicking on send button");
        btnSend.click();
    }

    public String getErrorMessage() {
        log.info("Getting error message text");
        WaitHelper.waitForVisibility(lblError);
        return lblError.getText();
    }

    public String getSuccessMessage() {
        log.info("Getting success message text");
        WaitHelper.waitForVisibility(lblSuccess);
        return lblSuccess.getText();
    }

    /**
     * Ta metoda odpowiada za załączenie obrazka cat.jpg z katalogu resources
     * Wpierw pobiera jego ścieżkę bezwzględną, tak aby test ten działał na każdym komputerze,
     * podobnie jak to zrobiliśmy ze ścieżką do drivera
     * Następnie przekazuje tę ścieżkę komendą sendKeys do pola dodawania załącznika
     * <p>
     * Jest to swego rodzaju trick - nie musimy obsługiwać systemowego okienka dodawania plików,
     * ponieważ Selenium nie ma jak się z nim połączyć.
     * <p>
     * Rozwiązanie to działa w większości przypadków tego typu elementów
     *
     * @return
     */
    public void attachCatImage() {
        log.info("Attaching cat image");
        ClassLoader classLoader = this.getClass().getClassLoader();
        String catImage = classLoader.getResource("cat.jpg").getFile();
        String absolutePathToCatImage = new File(catImage).getAbsolutePath();
        tfAttachFile.sendKeys(absolutePathToCatImage);
    }
}
