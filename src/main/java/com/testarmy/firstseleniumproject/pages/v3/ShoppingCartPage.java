package com.testarmy.firstseleniumproject.pages.v3;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import lombok.extern.java.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Log
public class ShoppingCartPage {

    @FindBy(id = "total_price")
    private WebElement lblTotalPrice;

    public ShoppingCartPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

    public String getTotalPrice() {
        log.info("Getting total price value");
        WaitHelper.waitForVisibility(lblTotalPrice);
        return lblTotalPrice.getText();
    }
}
