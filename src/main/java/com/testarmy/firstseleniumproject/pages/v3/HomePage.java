package com.testarmy.firstseleniumproject.pages.v3;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import lombok.extern.java.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Log
public class HomePage {

    @FindBy(className = "login")
    private WebElement btnSignIn;
    @FindBy(id = "contact-link")
    private WebElement btnContactUs;
    @FindBy(css = "ul#homefeatured > li:nth-child(2) a.product-name")
    private WebElement btnSecondProduct;

    public HomePage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

    public LoginPage clickOnSignInBtn() {
        log.info("Clicking on button sign in");
        btnSignIn.click();
        return new LoginPage();
    }

    public ContactUsPage clickOnContactUsBtn() {
        log.info("Clicking on button contact us");
        btnContactUs.click();
        return new ContactUsPage();
    }

    public ProductPage clickOnSecondProduct() {
        log.info("Clicking on second product");
        btnSecondProduct.click();
        return new ProductPage();
    }
}
