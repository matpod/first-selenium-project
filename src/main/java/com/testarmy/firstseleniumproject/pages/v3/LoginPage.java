package com.testarmy.firstseleniumproject.pages.v3;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import lombok.extern.java.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Log
public class LoginPage {

    @FindBy(id = "email")
    private WebElement tfEmail;
    @FindBy(id = "passwd")
    private WebElement tfPassword;
    @FindBy(id = "SubmitLogin")
    private WebElement btnLogin;
    @FindBy(css = "ol > li")
    private WebElement lblError;

    public LoginPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

    public LoginPage inputEmailAddress(String email) {
        log.info("Entering email address");
        WaitHelper.waitForVisibility(tfEmail);
        tfEmail.sendKeys(email);
        return this;
    }

    public LoginPage inputPassword(String password) {
        log.info("Entering password");
        WaitHelper.waitForVisibility(tfPassword);
        tfPassword.sendKeys(password);
        return this;
    }

    public HomePage clickOnLoginBtn() {
        log.info("Clicking on button login");
        btnLogin.click();
        return new HomePage();
    }

    public String getErrorMessage() {
        log.info("Getting error message text");
        WaitHelper.waitForVisibility(lblError);
        return lblError.getText();
    }
}
