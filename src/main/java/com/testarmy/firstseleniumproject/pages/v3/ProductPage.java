package com.testarmy.firstseleniumproject.pages.v3;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import com.testarmy.firstseleniumproject.config.WaitHelper;
import lombok.extern.java.Log;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Log
public class ProductPage {

    @FindBy(css = "button[name='Submit'] > span")
    private WebElement btnAddToCart;
    @FindBy(css = "a[title='Proceed to checkout']")
    private WebElement btnProceed;

    public ProductPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

    public ProductPage clickOnAddToCart() {
        log.info("Clicking on button add to cart");
        WaitHelper.waitForVisibility(btnAddToCart);
        btnAddToCart.click();
        return this;
    }

    public ShoppingCartPage clickOnProceed() {
        log.info("Clicking on button proceed to checkout");
        WaitHelper.waitForVisibility(btnProceed);
        btnProceed.click();
        return new ShoppingCartPage();
    }

}
