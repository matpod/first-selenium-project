package com.testarmy.firstseleniumproject.pages.v1;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage {

    @FindBy(css = "button[name='Submit'] > span")
    public WebElement btnAddToCart;
    @FindBy(css = "a[title='Proceed to checkout']")
    public WebElement btnProceed;

    public ProductPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

}
