package com.testarmy.firstseleniumproject.pages.v1;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    @FindBy(className = "login")
    public WebElement btnSignIn;
    @FindBy(id = "contact-link")
    public WebElement btnContactUs;
    @FindBy(css = "ul#homefeatured > li:nth-child(2) a.product-name")
    public WebElement btnSecondProduct;

    public HomePage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

}
