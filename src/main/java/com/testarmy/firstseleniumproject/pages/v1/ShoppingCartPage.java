package com.testarmy.firstseleniumproject.pages.v1;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage {

    @FindBy(id = "total_price")
    public WebElement lblTotalPrice;

    public ShoppingCartPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }

}
