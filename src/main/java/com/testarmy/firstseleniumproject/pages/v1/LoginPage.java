package com.testarmy.firstseleniumproject.pages.v1;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    @FindBy(id = "email")
    public WebElement tfEmail;
    @FindBy(id = "passwd")
    public WebElement tfPassword;
    @FindBy(id = "SubmitLogin")
    public WebElement btnLogin;
    @FindBy(css = "ol > li")
    public WebElement lblError;

    public LoginPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }
}
