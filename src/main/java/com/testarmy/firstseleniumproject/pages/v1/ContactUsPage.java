package com.testarmy.firstseleniumproject.pages.v1;

import com.testarmy.firstseleniumproject.config.DriverHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage {

    @FindBy(id = "id_contact")
    public WebElement ddSubject;
    @FindBy(id = "email")
    public WebElement tfEmail;
    @FindBy(id = "id_order")
    public WebElement tfOrderRef;
    @FindBy(id = "message")
    public WebElement tfMessage;
    @FindBy(id = "submitMessage")
    public WebElement btnSend;
    @FindBy(css = ".alert.alert-success")
    public WebElement lblSuccess;
    @FindBy(css = "ol > li")
    public WebElement lblError;

    public ContactUsPage() {
        PageFactory.initElements(DriverHelper.getDriver(), this);
    }
}
